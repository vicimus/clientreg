<?php

namespace Vicimus\ClientReg;

use Vicimus\ClientReg\Exceptions\HashcashException;

/**
 * This class is used to validate hashcash strings sent in from clients during
 * the registration request process.
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class Hashcash
{
    /**
     * The version of hashcash used
     *
     * @var integer
     */
    protected $version;

    /**
     * The number of bits required in this hashcash
     *
     * @var integer
     */
    protected $bits;

    /**
     * The date stamp when this hashcash was created
     *
     * @var string
     */
    protected $date;

    /**
     * The name of the resource being contacted (the service)
     *
     * @var string
     */
    protected $resource;

    /**
     * Part of the spec but not used
     *
     * @var null
     */
    protected $extension = '';

    /**
     * The random hash portion of the hashcash
     *
     * @var string
     */
    protected $rand;

    /**
     * The hashed counter portion of the hashcash
     *
     * @var string
     */
    protected $counter;

    /**
     * The entire hashcash received
     *
     * @var sring
     */
    protected $hashcash;

    /**
     * If there is an error or the hashcash is determined to be invalid, the
     * reason will be placed in this property.
     *
     * @var string
     */
    protected $error = null;

    /**
     * The name of the class used to store hashcash records
     *
     * @var string
     */
    protected $model = null;

    /**
     * Constructs an instance of the Hashcash class using the string received
     * from the client request.
     *
     * @param string $hashcash The hashcash string
     * @param string $model    The name of the class used to store hashcashes
     */
    public function __construct($hashcash, $model)
    {
        $this->hashcash = $hashcash;
        $this->model = $model;

        try {
            list(

                $this->version,
                $this->bits,
                $this->date,
                $this->resource,
                $this->extension,
                $this->rand,
                $this->counter

            ) = explode(':', $this->hashcash);
        } catch (\Exception $ex) {
            throw new HashcashException(
                'Invalid Hashcash formatting: '.$this->hashcash
            );
        }
    }

    /**
     * Get the last error
     *
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Determines if the hascash is valid or not
     *
     * @param string $model The class used to store HashcashRecords
     *
     * @return boolean
     */
    public function isValid($model)
    {
        //Basic validation
        if ($this->version != ClientRegistrationRequest::HASHCASH_VERSION) {
            return !$this->error = 'Invalid version';
        }

        if ($this->bits != ClientRegistrationRequest::HASHCASH_BITS) {
            return !$this->error = 'Invalid number of bits';
        }

        if ($this->resource != env('APP_DOMAIN')) {
            return !$this->error = 'Invalid resource specified';
        }

        $n = (int)ceil($this->bits / 4.0);

        //Check the digest bit length
        $digest = call_user_func(
            ClientRegistrationRequest::HASH_ALGORITHM_METHOD,
            $this->hashcash
        );

        $leadZeros = str_pad('', $n, '0');
        if (substr($digest, 0, $n) !== $leadZeros) {
            return !$this->error = 'Invalid hashcash comparison';
        }

        //Check the age of the hashcash
        $dateLength = strlen(ClientRegistrationRequest::HASHCASH_DATE_FORMAT);
        if (strlen($this->date) < $dateLength) {
            return !$this->error = 'Invalid date format';
        }

        $date = [
            'yy' => substr($this->date, 0, 2) + 2000,
            'mm' => substr($this->date, 2, 2),
            'dd' => substr($this->date, 4),
        ];
       
        $date = (new \DateTime(implode('-', $date)))->format('U');
        $now = (new \DateTime)->format('U');

        $maxLife = ClientRegistrationRequest::HASHCASH_LIFE * 24 * 60 * 60;
        if ($now - $date >= $maxLife) {
            return !$this->error = 'Hashcash has expired';
        }

        //Check the existence of this hashcash
        $stored = $model::find($this->hashcash);
        if ($stored) {
            throw new ClientRegistrationError(
                'Hashcash has already been used'
            );
        }

        $model::create([
            'hashcash' => $this->hashcash,
            'created'  => $now,
        ]);

        return true;
    }
}
