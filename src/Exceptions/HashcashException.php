<?php

namespace Vicimus\ClientReg\Exceptions;

/**
 * If there is an error validating or reading the hashcash sent in with the
 * ClientRegistrationRequest, this exception will be thrown.
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class HashcashException extends \Exception
{
    
}
