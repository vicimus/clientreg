<?php

namespace Vicimus\ClientReg\Exceptions;

/**
 * Indicates an error occured during registration
 *
 * @author Jordan Grieve
 */
class ClientRegistrationError extends \Exception
{

}
