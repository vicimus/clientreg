<?php

namespace Vicimus\ClientReg;

use Vicimus\ClientReg\Exceptions\ClientRegistrationError;

/**
 * This class is used to handle the Client Registration endpoint. Provides
 * all of the validation required.
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class ClientRegistrationRequest
{
    /**
     * Indicates which algorithm is used in the hashcash
     *
     * @var string (sha-1 or sha-256)
     */
    const HASH_ALGORITHM        = 'sha-1';

    /**
     * The name of the method associated with the above algorithm
     *
     * @var string (sha1 or sha256)
     */
    const HASH_ALGORITHM_METHOD = 'sha1';

    /**
     * The number of bits required in the hashcash (20 for production, or 10
     * for development)
     *
     * @var integer
     */
    const HASHCASH_BITS         = 10;

    /**
     * The version of hashcash this implementation is using
     *
     * @var integer
     */
    const HASHCASH_VERSION      = 1;

    /**
     * The number of days a hashcash remains valid before expiring
     *
     * @var integer
     */
    const HASHCASH_LIFE         = 3;

    /**
     * The date format expected in the hashcash
     *
     * @var string
     */
    const HASHCASH_DATE_FORMAT  = 'yymmdd';

    /**
     * Allow duplicate hashcashes (should always be false in production)
     *
     * @var boolean
     */
    const ALLOW_DUPLICATES      = false;

    /**
     * Stores the class name of the model used by the Client Registration
     *
     * @var string
     */
    protected $model = null;

    /**
     * These are the required properties that must be present when a client
     * registration request is being made. The index is the property required,
     * and the value is the maximum number of bytes.
     *
     * @var mixed[]
     */
    protected static $required = [
        'name'                  => 100,
        'accept_terms'          => null,
        'hashcash'              => 100,
    ];

    /**
     * These are optional properties that may be present, as well as the
     * maximum number of bytes allowed if they are present.
     *
     * @var mixed[]
     */
    protected static $optional = [
        'website'               => 200,
        'description'           => 500,
        'organization'          => 100,
        'redirect_uri_prefix'   => 200,
    ];

    /**
     * The constructor accepts an array of input, generally the input sent
     * through a curl request to request client registration. It validates
     * the inputand throws appropriate exceptions if neccessary.
     *
     * @param array  $input         The input from the registration request
     * @param string $model         The Eloquent model used to record Clients
     * @param string $hashcashModel The eloquent model used to record hashcash
     * @throws ClientRegistrationError if an error occurs duh
     */
    public function __construct(array $input, $model, $hashcashModel)
    {
        $this->model = $model;
        $this->hashcashModel = $hashcashModel;

        $this->processProperties($input);
        
        $this->hashcash = new Hashcash($this->hashcash, $this->hashcashModel);

        //Validate the hashcash
        if (!$this->hashcash->isValid($this->hashcashModel)) {
            throw new ClientRegistrationError(
                'Invalid hashcash value: '.$this->hashcash->getError()
            );
        }
        
        //Register the client
        $exists = $model::where('name', $this->name)->first();
        if ($exists && !self::ALLOW_DUPLICATES) {
            throw new ClientRegistrationError(
                'Client already exists with that name'
            );
        }
    }

    /**
     * Loops through the properties and checks for validity
     *
     * @param array $input The input from the registration request
     *
     * @return void
     */
    protected function processProperties(array $input)
    {
        foreach (self::$required as $property => $maximum) {
            //Check if required property is missing
            if (!array_key_exists($property, $input)) {
                throw new ClientRegistrationError(
                    'Missing required field ['.$property.']'
                );
            }

            //Check if under maximum bytes
            if ($maximum && strlen($input[$property]) > $maximum) {
                throw new ClientRegistrationError(
                    'Value of '.$property.' is over '.$maximum.' bytes'
                );
            }
        
            $this->$property = $input[$property];
        }

        foreach (self::$optional as $property => $maximum) {
            $this->$property = array_key_exists($property, $input)
                                ? $input[$property] : null;
        }
    }

    /**
     * Register the client with the service, creating a unique ID. This method
     * uses uses the attributes assigned during the constructor to register
     * the Client.
     *
     * @return Client
     */
    public function register()
    {
        $model = $this->model;

        while (true) {
            $clientID = bin2hex(openssl_random_pseudo_bytes(16));
            $clientSecret = bin2hex(openssl_random_pseudo_bytes(16));

            $exists = $model::find($clientID);
            if (!$exists) {
                return $model::create(
                    [
                    'id'            => $clientID,
                    'secret'        => $clientSecret,
                    'name'          => $this->name,
                    'website'       => $this->website,
                    'description'   => $this->description,
                    'organization'  => $this->organization,
                    ]
                );
            }
        }
    }

    /**
     * Generate a new ID and Secret Pair
     *
     * @param string $model The Eloquent based model used to store clients
     *
     * @return mixed[]
     */
    public static function generatePair($model)
    {
        while (true) {
            $clientID = bin2hex(openssl_random_pseudo_bytes(16));
            $clientSecret = bin2hex(openssl_random_pseudo_bytes(16));

            $exists = $model::find($clientID);
            if (!$exists) {
                return [
                    'id'            => $clientID,
                    'secret'        => $clientSecret,
                ];
            }
        }
    }

    /**
     * Get the client_registration_challenge string
     *
     * @return string
     */
    public static function getChallenge()
    {
        return implode(
            ':',
            [
            self::HASH_ALGORITHM,
            self::HASHCASH_BITS,
            env('APP_DOMAIN', 'localhost'),
            ]
        );
    }
}
